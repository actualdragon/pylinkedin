# Scrape My LinkedIn

This is an experiment to explain to individuals who ask me why I do not use my email address in public forums. It is for educational purposes only.

## Why snapshots?

Websites like linkedin are much easier to scrape using `JavaScript` and headless chrome thanks to the vulnerabilities of `emberjs` and the reactive-js-frameworks. The data-binding is a 1 to 1 reletationship (much like React, Angular, etc.) and in the case of a list, the DOM is not reused.

If you consider a site like Whatsapp web app, this won't work because whatsapp reuses the DOM and at any given point in time you can only scrape a maximum of 10 chat/comment entries. For websites like that you have to build an incremental scrape system.

To understand this better, read the post by [stefankrause](https://www.stefankrause.net/wp/?p=342)

## Licence

Scrape My LinkedIn is released under MIT license
