import csv
from bs4 import BeautifulSoup

def scrape_email(soup):
	""" get email address
	scrape from all hrefs with mailto
	"""
	emails = []
	mailtos = soup.select('a[href^=mailto]')

	for i in mailtos:
		if i.string != None:
			emails.append(i.string.strip())

	# remove duplicates
	return list(set(emails))

def scrape_profile(soup):
	""" get linkedin profile link
	scrape from href attribute in comments and replies
	"""
	profiles = []

	# comments
	for i in soup.find_all('a', attrs={'data-control-name' : 'comment_actor'}):
		profiles.append(i['href'])

	# replies in comments
	for j in soup.find_all('a', attrs={'data-control-name' : 'reply_actor'}):
		profiles.append(j['href'])

	# remove duplicates
	return list(set(profiles))

def scrape_all(soup):
	""" todo the works

	target user profile using
	data-control-name="comment_actor", data-control-name="reply_actor"

	create a table with the following headers
	name, title, linkedin_profile, email, qualitative_analysis_of_profile
	"""
	return 0

def print_csv(data, name):
	""" print data in list to csv
	todo spread attributes into table
	"""
	with open(name, 'w') as csv_file:
		writer = csv.writer(csv_file)

		for d in data:
			writer.writerow([d])

if __name__ == '__main__':
	soup = BeautifulSoup(open("./snapshots/comments.html"), "html.parser")

	email_data = scrape_email(soup)
	print_csv(email_data, "./bin/emails.csv")

	profile_data = scrape_profile(soup)
	print_csv(profile_data, "./bin/profiles.csv")

	# all_data = scrape_all(soup)
	# print_csv(all_data, "./bin/all.csv")
